#!/usr/bin/env bash

IMAGE_NAME='arp.reg.innertoon.com/library/php5-apache-ext'
CONTAINER_NAME="checkout"
CMD=`pwd`
echo ${CMD}


docker run --rm \
    -u root:root \
    -v ${CMD}:/var/www \
    -v ${CMD}/logs:/home/logs \
    -p 80:80 \
    --name ${CONTAINER_NAME} \
    ${IMAGE_NAME}