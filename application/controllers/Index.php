<?php

/**
 * 默认Controller
 * @description Yaf 入口文件
 * @author zhaowei
 * @version 2016-05-24
 */
class IndexController extends Controller
{
    //默认Action
    public function indexAction()
    {

        $db["username"] = "admin";
        $db["password"] = "admin";

        $admin = new  AdminModel();

        $return = $admin->listData('1=1');
        Fn::outputToJson(0, '请求成功', $return);
        exit;
    }
}