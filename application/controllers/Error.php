<?php
/**
 * 默认Controller
 * @description Yaf异常处理类
 * @author zhaowei
 * @version 2016-05-24
 */
class ErrorController extends Yaf_Controller_Abstract{
    //默认Action
    public function errorAction() {
        $exception = $this->getRequest()->getException();
        try {
            throw $exception;
        } catch (Yaf_Exception_LoadFailed $e) {
            header('HTTP/1.1 404 Not Found');
            header("status: 404 Not Found");
            
            exit;
        } catch (Yaf_Exception $e) {
            Fn::writeLog($exception->getMessage());
            
            header('HTTP/1.1 500 Internal Server Error');
            exit;
        }
        
       
    }

}