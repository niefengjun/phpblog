<?php
/**
 * Api接口公共Controller
 * @author lifuqiang
 *
 */
class ApiBaseController extends Controller {
    
    /**
     * 请求参数
     * @var array
     */
    protected $_postData          = array();
    
    public function init() {
        parent::init();
        
        //验证请求来源，防止跨过签名验证
        $_postData = strtolower($this->getRequest()->getRequestUri());
        
                
      //  $this->_postData = $this->getRequest()->getParams();
    }
    
    
}