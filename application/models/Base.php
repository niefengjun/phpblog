<?php
if (! defined("ERROR_PARAM")){
    define("ERROR_PARAM", 1001);        //　参数错误
	define("ERROR_SYS", 1002);          //  系统错误
	define("ERROR_IOGIC", 1003);        //  业务逻辑错误
}
class BaseModel
{
    public $_dbString    = '';
    public $_table       = '';
    
    public $_redisString = '';        //redis
    
    public $_errorNo     = 0;
    public $_errorMsg    = '';
    
    public $resErrorSys = false;
    
    public $table_status    = array();

    public $errorMsgConfig = array(
        ERROR_PARAM => "参数错误", 
        ERROR_SYS   => "系统错误", 
        ERROR_IOGIC => "数据逻辑错误"
    );
    
    function __construct(){}
    
    function regainError()
    {
        $this->_errorNo  = 0;
        $this->_errorMsg = '';
    }
    function setErrorNo($error_no, $errorMsg = "")
    {
        $this->_errorNo = $error_no;
        $this->_errorMsg = $errorMsg;
    }

    function getErrorNo()
    {
        return $this->_errorNo;
    }
    
    function getErrorMsg()
    {
        $msg = "";
        if (array_key_exists($this->_errorNo, $this->errorMsgConfig)) {
            $msg = $this->errorMsgConfig[$this->_errorNo];
        }
        
        if ($this->_errorMsg) {
            $msg .= ":" . $this->_errorMsg;
        }
        
        return $msg;
    }
  
    /**
     * 增加数据
     *   - 支持单条SQL语句，$row 需要为字符串型,$row必须是完整的sql语句，后台验证insert
     *   - 如果是sql
     *   - 只返回成功以否，不返回插入的自增ID
     *
     * @param string|array() $row  需要增加的表字段 		
     * @param string $table
     * @param string $dbString
     * @return int - 更新影响的行数 | false
     */
    protected function add($row, $replace = false,$getInsertId=false,$table='', $dbString='')
    {
        if (! $table){
            $table = $this->_table;
        }
        if (! $dbString){
            $dbString = $this->_dbString;
        }

        if (is_array($row)){

	        if (! $this->_encodeTableRow($row)){
	            print_r($row);
	            $this->setErrorNo(ERROR_PARAM);
	            return false;
	        }

//            print_r(is_array($row)) ;
//            print_r($row);
//            exit() ;
	        $result = Db::getInstance($dbString)->insert($table, $row, $replace, $getInsertId);
        } else {
            $result = Db::getInstance($dbString)->insertBySql($row);
        }
     
        if (false === $result){
            $this->setErrorNo(ERROR_SYS);
        }
        
        return $result;
    }
    
    /**
     * 增加数据(获取自增ID)
     *   - 支持单条SQL语句，$row 需要为字符串型,$row必须是完整的sql语句，后台验证insert
     *   - 如果是sql
     *   - 只返回成功以否，不返回插入的自增ID
     *
     * @param string|array() $row  需要增加的表字段
     * @param string $table
     * @param string $dbString
     * @return int - 更新影响的行数 | false
     */
    protected function addGetInsertId($row, $replace = false, $table='', $dbString='')
    {
        if (! $table){
            $table = $this->_table;
        }
        if (! $dbString){
            $dbString = $this->_dbString;
        }
    
        if (is_array($row)){
            if (! $this->_encodeTableRow($row)){
                $this->setErrorNo(ERROR_PARAM);
                return false;
            }
            $result = Db::getInstance($dbString)->insert($table, $row, $replace, TRUE);
        } else {
            $result = Db::getInstance($dbString)->insertBySql($row, TRUE);
        }
         
        if (false === $result){
            $this->setErrorNo(ERROR_SYS);
        }
    
        return $result;
    }
    
    /**
     * 更新操作
     * @param string|array $row
     * @param string $where
     * @param string $table
     * @param string $dbString
     * @return boolean|unknown  返回受影响的行数
     */
    protected function update($row, $where = '', $table='', $dbString=''){
        if (! $table){
            $table = $this->_table;
        }
        
        if (! $dbString){
            $dbString = $this->_dbString;
        }
        
        if (is_array($row)){
            if (! $this->_encodeTableRow($row)){
                $this->setErrorNo(ERROR_PARAM);
                return false;
            }
            $result = Db::getInstance($dbString)->update($table, $row, $where);
        } else {
            $result = Db::getInstance($dbString)->updateBySql($row);
        }
        
        if (false === $result){
            $this->setErrorNo(ERROR_SYS);
        }
        
        return $result;
    }
    
    /**
     * 删除数据，返回受影响的行数
     * @param unknown $where
     * @param string $table
     * @param string $dbString
     */
    protected function delete($where, $table='', $dbString=''){
    
        if (! $where) {
            $this->setErrorNo(ERROR_PARAM);
            return false;
        }
        
        if (! $table){
            $table = $this->_table;
        }
        if (! $dbString){
            $dbString = $this->_dbString;
        }
        
        $result = Db::getInstance($dbString)->delete($table, $where);
        if (false === $result){
            $this->setErrorNo(ERROR_SYS);
        }
        return $result;
    }
    
    /**
     * 获取单条数据
     * @param unknown $where
     * @param unknown $fields
     * @param string $order
     * @param string $table
     * @param string $dbString
     */
    protected function getRow($where, $fields = array(), $order = null, $table = null, $dbString = null, $master = false){
        if (! $where) {
            $this->setErrorNo(ERROR_PARAM);
            return array();
        }
        
        if (! $table){
            $table = $this->_table;
        }
        if (! $dbString){
            $dbString = $this->_dbString;
        }
        $result = Db::getInstance($dbString)->selectOne($table, $where, $fields, $order, $master);
        
        if (false === $result){
            $this->setErrorNo(ERROR_SYS);
            return array();
        }
        
        $this->_decodeTableRow($result);
        
        return $result;
    }
    
    /**
     * 获取多条数据
     * @param unknown $where
     * @param unknown $fields
     * @param string $order
     * @param string $limit
     * @param string $table
     * @param string $dbString
     */
    protected function getRows($where, $fields = array(), $order = null, $limit = null, $table = null, $dbString = null, $master = false){
        if (! $where) {
            $this->setErrorNo(ERROR_PARAM);
            return array();
        }
        
        if (! $table){
            $table = $this->_table;
        }
        if (! $dbString){
            $dbString = $this->_dbString;
        }
        
        $result = Db::getInstance($dbString)->selectAll($table, $where, $fields, $order, $limit, $master);
        
        if (false === $result){
            $this->setErrorNo(ERROR_SYS);
            return array();
        }
        
        foreach ($result as &$row) {
            $this->_decodeTableRow($row);
        }
        unset($row);
        
        return $result;
    }
    
    /**
     * 按照sql获取数据
     * @param unknown $sql
     * @param string $dbString
     * @return array
     */
    protected function getRowsBySql($sql, $dbString = '', $master = false) {
        if (! $sql) {
            $this->setErrorNo(ERROR_PARAM);
            return array();
        }
        
        if (! $dbString) {
            $dbString = $this->_dbString;
        }
        
        $result = Db::getInstance($dbString)->setlectBySql($sql, $master);
        
        if (false === $result) {
            $this->setErrorNo(ERROR_SYS);
            return array();
        }
        
        foreach ($result as &$row) {
            $this->_decodeTableRow($row);
        }
        unset($row);
        
        return $result;
    }
    
    /**
     * 按照sql获取单条数据
     * @param unknown $sql
     * @param string $dbString
     * @param string $master
     * @return array 
     */
    protected function getRowBySql($sql, $dbString = '', $master = false) {
        $result = $this->getRowsBySql($sql, $dbString, $master);
        
        if (empty($result)) {
            return array();
        }
        
        return $result[0];
    }
    
    /**
     * 从缓存中获取数据
     * @param unknown $mcKey
     * @param string $redisString
     */
    protected function getMcRow($mcKey, $redisString = '') {
        if (! $redisString) {
            $redisString = $this->_redisString;
        }
        
        $row = RedisClient::instance($redisString)->get($mcKey);
        
        if (false === $row) {
            return false;
        }
        
        if (! is_numeric($row)) {
            $row = unserialize($row);
        }
        
        return $row;
        
    }
    
    /**
     * 设置缓存
     * @param unknown $mcKey
     * @param unknown $row
     * @param number $time 若time＝0，表示永久缓存
     * @param unknown $redisString
     */
    protected function setMcRow($mcKey, $row, $time = 0, $redisString = '') {
        if (! $redisString) {
            $redisString = $this->_redisString;
        }
        
        if (! is_numeric($row)) {
            $row = serialize($row);
        }
        
        if ($time > 0) {
            return RedisClient::instance($redisString)->setex($mcKey, $time, $row);
        } else {
            return RedisClient::instance($redisString)->set($mcKey, $row);
        }
    }
    
    /**
     * 删除mc缓存
     * @param unknown $mcKey
     * @param string $redisString
     */
    protected function deleteMcRow($mcKey, $redisString = '') {
        if (! $redisString) {
            $redisString = $this->_redisString;
        }
        
        return RedisClient::instance($redisString)->del($mcKey);
    }
    
    
//-----------------------------------------内部方法-----------------------------------------//   
    
    /**
     * 解析数据库语句
     *
     * @param array() $row
     * @return bool
     */
    private function _encodeTableRow(&$row)
    {
        if (! $this->table_status || ! $row){
            return true;
        }
        foreach ($row as $k=>$v){
            if (! isset($this->table_status[$k])){
                return false;
            }
    
            if ($this->table_status[$k] == 'i'){
                $row[$k] = $v+0;
            }
            elseif ($this->table_status[$k] == 's'){
                $row[$k] = $v . '';
            }
            elseif ($this->table_status[$k] == 'x'){
                if (! is_array($v)){
                    return false;
                }
                $row[$k] = json_encode($v);
            }
        }
    
        return true;
    }
    
    /**
     * 解析从数据库中获取的数据
     *     - 需要配置$table_status 变量，该变量定义了
     *
     * @param array() $row
     * @return array()
     */
    private function _decodeTableRow(&$row)
    {
        if (! $this->table_status || ! is_array($row) || ! $row){
            return true;
        }
        foreach ($row as $k=>$v){
            $type = isset($this->table_status[$k]) ? $this->table_status[$k] : '';
            if ($type)
            {
                if ($this->table_status[$k] == 'x'){
                    if (! $v){
                        $row[$k] = array();
                    }
                    else{
                        $row[$k] = json_decode($v, true);
                    }
                }
            }
        }
    
        return true;
    }
}
?>