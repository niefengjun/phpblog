<?php
class OrgauthModel extends BaseModel
{
    public $_table         = 'daily_org_oauth';
    public $_dbString      = 'daily';
    public $_redisString   = 'main';
    public $_mcPrefix      = 'OrgauthModel::';
    
    public $table_status   = [
        'id'            => 'i',
        'orgId'         => 'i',
        'appAToken'     => 's',
        'rToken'        => 's',
        'accessToken'   => 's',
        'buyTm'         => 'i',
        'updateTm'      => 'i',
    ];
    
    
    public function addData($row)
    {
        $row['buyTm'] = time();
        $row['updateTm'] = time();
        
        return $this->add($row);
    }
    
    public function updateInfoByOrgId(array $row, $orgId)
    {
        $row['updateTm'] = time();
        
        return $this->update($row, "orgId={$orgId}");
    }
    
    public function getRTokenByOrgId($orgId)
    {
        $orgId = intval($orgId);
        
        $row = $this->getRow("orgId={$orgId}");
        
        if (empty($row)) {
            return false;
        }
        
        return $row['rToken'];
    }
    
}