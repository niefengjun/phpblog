<?php

class Comment{
    /*
     * 添加评论
     */
    public static function addComment($appId,$secret,$subjectId,$toon_uid,$content,$toUserId,$toId,&$errMsg){
        $params = [
            'appId'=> intval($appId),
            'Token'=> Fn::generateToken($secret,true),
            'subjectId'=>$subjectId,
            'userId'=> $toon_uid,
            'content'=>"$content",
            'toUserId'=>$toUserId,
            'toId'=>$toId,
        ];
        $queryUrl = Yaf_Registry::get('appConfig')->get('ms.comment.apiurl.comment');
        if (! $queryUrl) {
            $errMsg = '论坛微服务评论列表接口url不存在';
            return false;
        }
        $result = self::_netPost($queryUrl, $params,'post', $errMsg);
        
        if (! $result) {
            return false;
        }
        
        return $result;
    }
    
    /*
     * 评论列表
     */
    public static function listComment($appId,$secret,$subjectId,$userId,$page,$pageLimit,&$errMsg){
        $offset = ($page-1)*$pageLimit+1;
        $params = [
            'appId'=> intval($appId),
            'subjectId'=>$subjectId,
            'userId' => $userId,
            'token' => Fn::generateToken($secret, true),
            'offset' => $offset,
            'limit' => $pageLimit
        ];
        $queryUrl = Yaf_Registry::get('appConfig')->get('ms.comment.apiurl.comment');
        if (! $queryUrl) {
            $errMsg = '论坛微服务评论列表接口url不存在';
            return false;
        }
        $result = self::_netPost($queryUrl, $params, 'get', $errMsg);
        if(!$result){
            return false;
        }
        return $result;
    }

    
    /*
     * 点赞接口
     */
    public static function like($appId,$secret,$subjectId,$toon_uid,$toId,&$errMsg){
        $param = [
            'appId'=> intval($appId),
            'Token'=> Fn::generateToken($secret, true),
            'subjectId'=>$subjectId,
            'userId' =>$toon_uid,
            'type' => 1,
            'toId' => $toId
        ];
        $queryUrl = Yaf_Registry::get('appConfig')->get("ms.comment.apiurl.like");
        if (! $queryUrl) {
            $errMsg = '论坛微服务点赞接口url不存在';
            return false;
        }
        $result = self::_netPost($queryUrl, $param, 'post', $errMsg);
        if(!$result){
            return false;
        }
        return true;
    }
    
    /*
     * 取消点赞接口
     * userId=397403&subjectId=20&cId=641&appId=2&token=784e6be99ed04457c2c446b7b635c7b7
     */
    public static function delLike($appId,$secret,$subjectId,$userId,$cId,&$errMsg){
        $token = Fn::generateToken($secret, true);
        $queryUrl = Yaf_Registry::get('appConfig')->get("ms.comment.apiurl.like");
        if (! $queryUrl) {
            $errMsg = '论坛微服务点赞接口url不存在';
            return false;
        }
        $url  = $queryUrl.'?userId='.intval($userId).'&subjectId='.intval($subjectId).'&cId='.$cId.'&appId='.intval($appId).'&token='.$token;
        $result = self::_netPost($url, array(), 'delete', $errMsg);
        if(!$result){
            return false;
        }
        return true;
    }
    /*
     * 网络请求接口
     * 接口返回格式 ['Code'=>状态码,'Msg'=>消息体,'Res'=>结果集]
     */
    private static function _netPost($queryUrl, array $params,$method, &$errMsg) {
        $result = Curl::callWebServer($queryUrl, $params, $method, 5, true,false);
        if ($result) {
            $resultData = json_decode($result, true);
            if (is_array($resultData) && isset($resultData['Code']) && $resultData['Code'] == 0) {
        
                return $resultData['Res'] === null || $resultData['Res'] == '' ? true : $resultData['Res'];
            }
        
            $errMsg = isset($resultData['Msg']) ? $resultData['Msg'] : '网络错误';
        } else {
            $errMsg = '网络错误';
        }
        
        Fn::writeLog("[Cmt] queryUrl={$queryUrl}\tparams=" . var_export($params, true) . "\tresult={$result}");
        
        return array();
    }
}