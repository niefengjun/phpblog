<?php
class Request extends Yaf_Request_Http {
    
    /**
     * 获取json请求
     * @return array
     */
    public static function getJsonRequest() {
        $data = file_get_contents('php://input');
        
        if (! $data) {
            return array();
        }
        
        return json_decode($data, true);
    }
}