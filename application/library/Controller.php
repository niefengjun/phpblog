<?php 
 /**
 * Controller 父类
 * @description 所有controller实现都需要继承该类
 * @author zhaowei
 * @version 2016-05-24
 */
class Controller extends Yaf_Controller_Abstract {
    
    protected   $_cnf           = null;
    protected   $_dispatcher    = null;
    
    protected   $_debug         = false;  // 是否开启调试
    
    public function init() {
       //初始化配置对象
       $this->_cnf          = Yaf_Registry::get('appConfig');
       //初始化Dispatcher对象
       $this->_dispatcher   = Yaf_Application::app()->getDispatcher();
       //关闭自动渲染
       $this->_dispatcher->disableView();  
	   //获取请求的request对象
	   $this->request  = $this->getRequest();

    }
    
    /**
     * 安全检查方法
     */
    public function chkHealthAction() {
        header("Cache-control:max-age=86400");
        die('ok');
    }
    
    /**
     * 校验签名
     *     将请求参数数组传递过来即可，若返回false，即校验失败 
     * @param array $params
     * @return boolean
     */
    protected function checkAppSign($params = array()) {
        
        if (empty($params) || empty($params['appId']) || empty($params['appSign'])) {
            return false;
        }
        
        $appSign = $params['appSign'];
        unset($params['appSign']);
        
        //获取应用配置信息
        if (! isset(Act_AppConfig::$appConfig[$params['appId']])) {
            Fn::writeLog("应用配置请求错误: appId=" . $params['appId']);
            
            return false;
        }
        
        $appSecret = Act_AppConfig::$appConfig[$params['appId']]['appSecret'];
        
        ksort($params);
        
        $combString = '';
        foreach ($params as $key=>$val) {
            $combString .= $key.$val;
        }
        
        $ret = strtoupper(md5($appSecret . $combString . $appSecret)) === $appSign;
        
        if (APP_ENV == 'develop' && $ret === false) {
            Fn::writeLog("===================", 'shenquntoon.log');
            Fn::writeLog("params:" . var_export($params, true), 'shequntoon.log');
            Fn::writeLog("appSign:\t" . $appSign, 'shequntoon.log');
            Fn::writeLog("myOut:\t" . strtoupper(md5($appSecret . $combString . $appSecret)), 'shequntoon.log');
            Fn::writeLog("===================", 'shequntoon.log');
        }
        
        return $ret;
    }
    
    
        
    
}
