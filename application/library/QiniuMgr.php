<?php
require_once __DIR__ . '/Qiniu/autoload.php';
// 引入鉴权类
use Qiniu\Auth;
// 引入上传类
use Qiniu\Storage\UploadManager;

class QiniuMgr{
    // 需要填写你的 Access Key 和 Secret Key
    protected static $accessKey = QINIU_ACCESSKEY;
    protected static $secretKey = QINIU_SECRETKEY;
    // 要上传的空间
    protected static  $bucket   = QINIU_BUCKET;

    protected $auth;

    public function __construct(){

        // 构建鉴权对象
        $this->auth = new Auth(self::$accessKey, self::$secretKey);
    }




    public function uploadFile($key, $file_path){
        $result = false;

        // 生成上传 Token
        $token = $this->auth->uploadToken(self::$bucket);

        // 要上传文件的本地路径
        // $filePath = 'F:\xampp\htdocs\APR\portal\1.0\admin\public\detail_img.png';

        // 上传到七牛后保存的文件名
        // $key = 'my-detail_img.png';

        // 初始化 UploadManager 对象并进行文件的上传。
        $uploadMgr = new UploadManager();

        // 调用 UploadManager 的 putFile 方法进行文件的上传。
        list($ret, $err) = $uploadMgr->putFile($token, $key, $file_path);
        if ($err !== null) {
            $result = $err;
        } else {
            $result = $ret;
        }
        return $result;
    }
}