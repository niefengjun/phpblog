<?php
/**
 * 公用函数类
 * @description 包含所有非业务相关的所有公共函数
 * @author zhaowei
 * @version 2016-05-24
 */
class Fn {
    
    const ALARM_SMS     = 1;
    const ALARM_MAIL    = 2;
    const ALARM_LOG     = 3;
    
    /*
     * 根据传入参数获取pdo数据类型
     *
     * @param string $val 传入值
     * @return string 
     */
	public static function select_datatype( $val ) {
	    
		if ( is_string( $val ) )  return PDO::PARAM_STR;
		if ( is_int( $val ) )     return PDO::PARAM_INT;
		if ( is_null( $val ) )    return PDO::PARAM_NULL;
        if ( is_bool( $val ) )    return PDO::PARAM_BOOL;
        return PDO::PARAM_STR;
	}
    
    
    /*
     * 显示调试信息
     *
     * @param string $str 传入值 
     */
    public static function writeLog( $str , $fileName = '' ) {
        if (! $fileName) {
            $fileName =  'error.log';
        } 

        Log::write($str, Log::INFO, $fileName);    
//         self::writeStdOut( $fileName,  $str ); 
    }
    
    /**
     * 通过CURL库进POST数据提交
     *
     * @param string $postUrl  url address
     * @param array $data  post data
     * @param int $timeout connect time out
     * @param bool $debug 打开 header 数据
     * @return string
     */
    public static function curlPost( $postUrl, $data = '', $timeout = 30, $header = [], $debug = false ) {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $postUrl );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HEADER, $debug );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
        curl_setopt( $ch, CURLINFO_HEADER_OUT, $debug );
        curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );

        $result = curl_exec( $ch );
        $info   = curl_getinfo( $ch );   //用于调试信息
        curl_close( $ch );

        if ( $result === false ) {
            self::writeLog( json_encode( $info ) );
            return false;
        }
        return trim( $result );
    }
    
    
   
    
    /**
     * 获取url返回值，curl方法
     */
    public static function curlGet( $url, $timeout = 1 ) {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
        
        $ret  = curl_exec( $ch );
        $info = curl_getinfo($ch);   //用于调试信息
        curl_close($ch);
        
        if ( $ret === false ) {
            self::writeLog( json_encode( $info ) );
            return false;
        }
        return trim( $ret );
    }

	public static function curlGetWithHeader( $url,$header, $timeout = 1 ) {
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POST, false);

		$ret  = curl_exec( $ch );
		$info = curl_getinfo($ch);   //用于调试信息
		curl_close($ch);

		if ( $ret === false ) {
			self::writeLog( json_encode( $info ) );
			return false;
		}
		return trim( $ret );
	}
  
    /**
     * 二维数组按指定的键值排序
     */
    public static function array_sort( $array, $keys, $type='asc' ) {
        if( !isset( $array ) || !is_array( $array ) || empty( $array ) ) return '';
        if( !isset( $keys ) || trim( $keys ) == '' ) return '';
        if( !isset( $type ) || $type == '' || !in_array( strtolower( $type ), array( 'asc', 'desc' ) ) ) return '';
        
        $keysvalue  = [];
        foreach( $array as $key => $val ) {
            $val[ $keys ]   = str_replace( '-', '', $val[ $keys ] );
            $val[ $keys ]   = str_replace( ' ', '', $val[ $keys ] );
            $val[ $keys ]   = str_replace( ':', '', $val[ $keys ] );
            $keysvalue[]    = $val[ $keys ];
        }
        
        asort( $keysvalue ); //key值排序
        reset( $keysvalue ); //指针重新指向数组第一个
        foreach( $keysvalue as $key => $vals ) 
            $keysort[] = $key;
        
        $keysvalue  = [];
        $count      = count( $keysort );
        if( strtolower( $type ) != 'asc' ) {
            for( $i = $count - 1; $i >= 0; $i-- ) 
                $keysvalue[] = $array[ $keysort[ $i ] ];
        }else{
            for( $i = 0; $i < $count; $i++ )
                $keysvalue[] = $array[ $keysort[ $i ] ];
        }
        return $keysvalue;
    }
    
    /**
    * 从结果集中总取出last names列，用相应的id作为键值
    * 原数据:
    * $records = array(
        array(
            'id' => 2135,
            'first_name' => 'John',
            'last_name' => 'Doe',
        ),
      );
    * array_column($records, 'last_name', 'id');
    * 结果:
    * Array
        (
            [2135] => Doe
            [3245] => Smith
            [5342] => Jones
            [5623] => Doe
        )
     */
    public static function array_column($input, $columnKey, $indexKey = NULL)
    {
        $columnKeyIsNumber = (is_numeric($columnKey)) ? TRUE : FALSE;
        $indexKeyIsNull = (is_null($indexKey)) ? TRUE : FALSE;
        $indexKeyIsNumber = (is_numeric($indexKey)) ? TRUE : FALSE;
        $result = array();

        foreach ((array)$input AS $key => $row)
        {
            if ($columnKeyIsNumber)
            {
                $tmp = array_slice($row, $columnKey, 1);
                $tmp = (is_array($tmp) && !empty($tmp)) ? current($tmp) : NULL;
            }
            else
            {
                $tmp = isset($row[$columnKey]) ? $row[$columnKey] : NULL;
            }
            if ( ! $indexKeyIsNull)
            {
                if ($indexKeyIsNumber)
                {
                    $key = array_slice($row, $indexKey, 1);
                    $key = (is_array($key) && ! empty($key)) ? current($key) : NULL;
                    $key = is_null($key) ? 0 : $key;
                }
                else
                {
                    $key = isset($row[$indexKey]) ? $row[$indexKey] : 0;
                }
            }

            $result[$key] = $tmp;
        }

        return $result;
    }
    
    /**
     * 内容审核接口
     * @params $content String
     * @params $projectName String 
     */
    public static function audiing_logs( $content, $projectName ) {
        
        $log        = '/home/logs/' . $projectName . '-auditing.log';
        $countFile  = '/home/logs/count';
        $fileSize   = 0;
        $maxSize    = 1024 * 1024 * 10;
        
        if( file_exists( $log ) )  $fileSize    = filesize( $log );
        if( $fileSize >= $maxSize ) {
            if( file_exists('/home/logs/count') ) {
                $count  = file_get_contents( $countFile );
                if( $count != false )
                    $newFile    = $log . '.' . $count;
            }
            else {
                $newFile    = $log . '.0';
            } 
            file_put_contents( $countFile, intval( $count ) + 1 );
            rename( $log, $newFile );
        }
        file_put_contents( $log, $content . "\n\n", FILE_APPEND );
    }
    
    public static function getUuid( $prifix = '' ) {
        $chars  = md5( uniqid( mt_rand(), true ) );
        $uuid   = substr( $chars, 0, 8 ) . '-';
        $uuid   .= substr( $chars, 8, 4 ) . '-';
        $uuid   .= substr( $chars, 12, 4 ) . '-';
        $uuid   .= substr( $chars, 16, 4 ) . '-';
        $uuid   .= substr( $chars, 20, 12 );
        return $prifix . $uuid;
    }
    
    /**
     * 根据经纬度计算两地距离， 返回单位为米
     * @param unknown $lat1
     * @param unknown $lng1
     * @param unknown $lat2
     * @param unknown $lng2
     * @return number
     */
    public static function getDistance($lat1, $lng1, $lat2, $lng2) {
        //地球半径
        $R = 6378137;
        //将角度转为狐度
        $radLat1 = deg2rad($lat1);
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);
        //结果
        $s = acos(cos($radLat1)*cos($radLat2)*cos($radLng1-$radLng2)+sin($radLat1)*sin($radLat2))*$R;
        //精度
        $s = round($s* 10000)/10000;
        return  round($s);
    }
    
    /**
     * 获取微妙
     * @return number
     */
    public static function getMillisecond() {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }
    
    /**
     * 输出json数据
     * @param unknown $code
     * @param unknown $message
     * @param string $data
     * @param string $noCache
     */
    public static function outputToJson( $code, $message, $data = null, $noCache = true ) {
        header("Content-type: application/json; charset=utf-8");
        
        if ( $noCache ) {
            header("Cache-Control: no-cache");
        }

        $msg = array(
            'meta' => array(
                'code' => $code,
                'message'  => $message, 
                'timestamp' => self::getMillisecond(),
            ),
            'data' => $data
        );
        
        $msg = json_encode($msg);
        
        header('Content-Length:' . strlen($msg));
        echo $msg;
        exit(0);        
    }
    
    /**
     * 计算字符长度，包括中文
     *
     * @param unknown_type $String
     * @return unknown
     */
    public static function getStrLen($str)
    {
        $I = 0;
        $StringLast = array();
        $Length = strlen($str);
        while ( $I < $Length ) {
            $StringTMP = substr($str, $I, 1);
            if (ord($StringTMP) >= 224) {
                if ($I + 3 > $Length) {
                    break;
                }
                $StringTMP = substr($str, $I, 3);
                $I = $I + 3;
            }
            elseif (ord($StringTMP) >= 192) {
                if ($I + 2 > $Length) {
                    break;
                }
                $StringTMP = substr($str, $I, 2);
                $I = $I + 2;
            }
            else {
                $I = $I + 1;
            }
            $StringLast[] = $StringTMP;
        }
    
        return count($StringLast);
    }
    
    /**
     * 截取字符串
     * @param unknown $string
     * @param unknown $length
     * @param string $append
     * @return unknown|string
     */
    public static function subStrForAllEnc($string, $length, $append = false)
    {
        if (strlen($string) <= $length) {
            return $string;
        }
        else {
            $I = 0;
            $J = 0;
            $strLen = strlen($string);
            while ( $I < $length && $J < $strLen ) {
                $StringTMP = substr($string, $J, 1);
                if (ord($StringTMP) >= 224) {
                    if ($J + 3 > $strLen) {
                        break;
                    }
                    $StringTMP = substr($string, $J, 3);
                    $J = $J + 3;
                }
                elseif (ord($StringTMP) >= 192) {
                    if ($J + 2 > $strLen) {
                        break;
                    }
                    $StringTMP = substr($string, $J, 2);
                    $J = $J + 2;
                }
                else {
                    $J = $J + 1;
                }
                $StringLast[] = $StringTMP;
                $I++;
            }
            $StringLast = implode("", $StringLast);
            if ($StringLast != $string && $append) {
                $StringLast .= "...";
            }
            return $StringLast;
        }
    }
    

    /**
     * 验证必填参数
     * @param array $params 参数一维数组
     * @param boolen $result
     */
    public static function checkNecessaryParams($params){
        $result = true;

        foreach ($params as $k => $v) {
            if(empty($v)){
                $result = false;
                self::writeLog(date("Y-m-d H:i:s")."\n".$k."必填\n");
                break;
            }
        }

        return $result;
    }
    
    
    /**
     * 报警函数
     * @param unknown $level    报警级别
     * @param unknown $title    报警title
     * @param unknown $msg      报警详情
     */
    public static function alarm( $level, $title, $msg ) {
    
    }
    
    
    /**
     * 发邮件
     * @param string|array $mailTo 接收邮件方  例如：xxx@163.com 、 array('xxx@163.com', 'xxx@qq.com')
     * @param string $title 邮件主题
     * @param string $content 邮件内容，可以保护html代码
     * @param string $attachment 附件路径
     * @param string $content_type 邮件类型 包括：TEXT|HTML
     * @param string $charset 编码
     * @author liweiwei
     */
    public static function sendMail($mailTo, $title, $content, $attachment = null, $content_type='TEXT', $charset='utf8')
    {
        require_once APP_PATH . '/application/library/PHPMailer/PHPMailerAutoload.php';
        
        $content_type = strtoupper($content_type);
    
        $mail = new PHPMailer(); // 实例化phpmailer
    
        $mail->IsSMTP(); // 设置发送邮件的协议：SMTP
    
        $mail->Host         = '172.28.2.240'; // 发送邮件的服务器  例如：smtp.exmail.qq.com、smtp.sina.cn、smtp.163.com
        $mail->Port = 25;
        $mail->SMTPAuth     = true; // 打开SMTP
        $mail->Username     = ''; // SMTP账户
        $mail->Password     = ''; // SMTP密码
        $mail->SMTPSecure   = 'ssl';
        
        if ($content_type == 'HTML') {
            $mail->IsHTML(true);
        } //是否使用HTML格式
    
        $mail->From     = '';
        $mail->FromName = '系统';
    
        // 添加多个发送人
        if (is_array($mailTo)) {
            foreach ($mailTo as $val) {
                $mail->AddAddress($val);
            }
        } else {
            $mail->AddAddress($mailTo);
        }
    
        if ($attachment) {
            $mail->AddAttachment($attachment); // 设置附件，服务器路径
        }
    
        //设置字符集编码
        if ($charset != 'utf8') {
            $mail->CharSet = $charset;
        } else {
            $mail->CharSet = "UTF-8";
        }
    
        $mail->Subject = "=?UTF-8?B?" . base64_encode($title) . "?=";
    
        //邮件内容（可以是HTML邮件）
        $mail->Body = $content;
    
        if (! $mail->send()) {
            Fn::writeLog('发送邮件失败: '.$mail->ErrorInfo);
            return false;
        } 
        
        return true;
    }
    
    /**
     * 获取userAgent
     * @return Ambigous <string, unknown>
     */
    public static function getHttpUserAgent() {
        return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }
    
    
    
    
    /**
     * 向标准输出写日志 K8s用
     * @param unknown $file
     * @param unknown $content
     */
    public static function writeStdOut( $file, $content ) {
        $fh = fopen('php://stdout', 'w');
        fwrite($fh, $content."\n");
        fclose($fh);
    }
    
    /**
     * 向标准错误输出写日志 K8s用
     * @param unknown $file
     * @param unknown $content
     */
    public static function writeStdErr( $file, $content ) {
        $fh = fopen('php://stdout', 'w');
        fwrite($fh, $content."\n");
        fclose($fh);
    }
    
    public static function checkToken($secret, $token) {
//         return true;
        
        $ua = self::getHttpUserAgent();
        $tm = date('YmdH');
        
        return $token === md5($secret . $ua . $tm . $secret);
    }
    
    /**
     * 校验手机号
     * @param unknown $mobile
     * @return boolean
     */
    public static function isMobile($mobile) {
        if (!is_numeric($mobile)) {
            return false;
        }
        return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
    }
    
    
    public static function generateToken($secret, $isService = false) {
        $ua = '';
        if (! $isService) {
            $ua = self::getHttpUserAgent();
            $ua = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36';
        }
        $tm = date('YmdH');
        return md5($secret . $ua . $tm . $secret);
    }
}