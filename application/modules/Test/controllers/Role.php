<?php
/**
 * Created by PhpStorm.
 * User: xulizhe
 * Date: 2017/5/11
 * Time: 上午11:19
 */
class RoleController extends Controller{

    public function listAction()
    {
        $params = -1;
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $errMsg = "";
        $result = QiToon::getRoleSimplelist($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function listPageAction()
    {
        $params = 1475148557166943;
//        $curPage = 1;
//        $pageSize = 2;
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $errMsg = "";
        $result = QiToon::getRoleSimplelistPage($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function detailAction()
    {
        $accessToken = Act_QtOauth::getAccessToken(55465);
//        $params["ids"] = "1,2";
        $params= "1,2";
        $errMsg = "";
        $result = QiToon::getRoleDetail($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function delAction()
    {
        $params = 123;
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $errMsg = "";
        $result = QiToon::delRole($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function addAction()
    {
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $params = array(
            'extAttr' => '111',
            'externalSysCode' => '111',
            'externalSysType' => '111',
            'ordinal' => 0,
            'belongRoleId' => 0,
            'roleCode' => '111',
            'roleFullName' => '111',
            'roleName' => 'web前端部',
            'roleType' => '111',
            'parentId' => 1475148557166943,
        );
        $errMsg = "";
        $result = QiToon::createRole($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function updateAction()
    {
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $params = array(
            'extAttr' => '111',
            'externalSysCode' => '111',
            'externalSysType' => '111',
            'ordinal' => 0,
            'belongRoleId' => 0,
            'roleCode' => '1111',
            'roleFullName' => '111',
            'roleName' => 'web前端部',
            'roleType' => '1111',
            'parentId' => 0,
            'id' => 111
        );
        $errMsg = "";
        $result = QiToon::updateRole($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

}