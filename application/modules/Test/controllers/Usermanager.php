<?php

/**
 * Created by PhpStorm.
 * User: 145927
 * Date: 2017/5/9
 * Time: 18:12
 */
class UsermanagerController extends controller {
	/*
	 * {"birthday":0,
	 * "certCode":"",
	 * "certType":"",
	 * "depts":[{"id":0,"ordinal":0,"relationType":"","userType":""}],
	 * "email":"",
	 * "extAttr":"",
	 * "externalSysCode":"",
	 * "externalSysType":"","firstName":"","language":"","lastName":"","mobilePhone":"","nation":"",
	 * "nativePlace":"",
	 * "officeLocation":"",
	 * "photo":"",
	 * "roles":[{"id":0,"ordinal":0,"relationType":""}],
	 * "sex":"",
	 * "userAccount":"","userCode":"","userName":"","userType":""}
	 * */
	public function createUserAction() {
		$data['mobilePhone'] = '18701663333';
		$data['birthday'] = '0';
		$data['certType'] = '';
		$data['depts'][0]['id'] = '1';
		$data['depts'][0]['ordinal'] = '1';
		$data['depts'][0]['relationType'] = '1';
		$data['depts'][0]['userType'] = '1';
		$data['email'] = '';
		$data['extAttr'] = '';
		$data['externalSysCode'] = '';
		$data['externalSysType'] = '';
		$data['firstName'] = 'asdfa';
		$data['language'] = 'zh-en';
		$data['lastName'] = 'asdf';
		$data['nation'] = '';
		$data['nativePlace'] = '';
		$data['officeLocation'] = '';
		$data['photo'] = '';
		$data['roles'][0]['id'] = '';
		$data['roles'][0]['ordinal'] = '';
		$data['roles'][0]['relationType'] = '';
		$data['sex'] = '';
		$data['userAccount'] = '';
		$data['userCode'] = '';
		$data['userName'] = '张三胖';
		$data['userType'] = '';
		$result = QiToon::createUser($data, $header);
		print_r($result);

	}

	public function updateUserAction() {
		$data['id'] = 925;
		$data['mobilePhone'] = '18701663333';
//		$data['birthday'] = '0';
		$data['certType'] = '';
//		$data['depts'][0]['id'] = '1';
//		$data['depts'][0]['ordinal'] = '1';
//		$data['depts'][0]['relationType'] = '1';
//		$data['depts'][0]['userType'] = '1';
		$data['email'] = 'zhangsanpang@syswin.com';
		$data['extAttr'] = '';
		$data['externalSysCode'] = '';
		$data['externalSysType'] = '';
		$data['firstName'] = 'asdfa';
		$data['language'] = 'zh-en';
		$data['lastName'] = 'asdf';
		$data['nation'] = '';
		$data['nativePlace'] = '';
		$data['officeLocation'] = '';
		$data['photo'] = '';
//		$data['roles'][0]['id'] = '';
//		$data['roles'][0]['ordinal'] = '';
//		$data['roles'][0]['relationType'] = '';
		$data['sex'] = '';
		$data['userAccount'] = '';
		$data['userCode'] = '';
		$data['userName'] = '张三胖';
		$data['userType'] = '';
		$errMsg = '';
		$result = QiToon::updateUser($data, $errMsg);
		if($errMsg){
			print_r($errMsg);
		}
		print_r($result);

	}

	public function getAllUsersAction() {
		$errMsg = '';
		$result = QiToon::getAllUsers($errMsg,55465);
		echo "<pre>";
		print_r($result);

	}

	public function delUserAction() {
		$errMsg = '';
		$result = QiToon::delUser($errMsg,'0',55465);
		echo "<pre>";
		print_r($result);
	}

	public function getAllUsersPageAction() {
		$errMsg = '';
		$result = QiToon::getAllUserPage($errMsg,55465);
		echo "<pre>";
		print_r($result);
	}

	public function getDeleteUsersAction() {
		$errMsg = '';
		$result = QiToon::getDeleteUsers($errMsg,55465);
		echo "<pre>";
		print_r($result);
	}

	public function getDeleteUsersPageAction() {
		$errMsg = '';
		$result = QiToon::getDeleteUsersPage($errMsg,55465);

		echo "<pre>";
		print_r($result);
	}

	public function getUserDetailAction() {
		$errMsg = '';
		$ids['ids'] = '918,928';
		$result = QiToon::getUserDetail($errMsg,$ids,55465);
		echo "<pre>";
		print_r($result);
	}

	public function getUsersByOrganunitAction() {
		$errMsg = '';
		$result = QiToon::getUsersByOrganunit($errMsg,55465,'0');
		echo "<pre>";
		print_r($result);
	}

	public function getUsersByOrganunitPageAction() {
		$errMsg = '';
		$result = QiToon::getUsersByOrganunitPage($errMsg,55465,0);
		echo "<pre>";
		print_r($result);
	}

	public function getUsersByRoleAction() {
		$errMsg = '';
		$result = QiToon::getUsersByRole($errMsg,55465,0);
		echo "<pre>";
		print_r($result);
	}

	public function getUsersByRolePageAction() {
		$errMsg = '';
		$result = QiToon::getUsersByRolePage($errMsg,55465,0);
		echo "<pre>";
		print_r($result);
	}

}