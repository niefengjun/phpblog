<?php
/**
 * Created by PhpStorm.
 * User: xulizhe
 * Date: 2017/5/11
 * Time: 上午11:19
 */
class OrganUnitController extends Controller{

    public function detailAction()
    {
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $params["ids"] = "1475148557166943,1475148557166941";
        $errMsg = "";
        $result = QiToon::getOrganUnitDetail($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function addAction()
    {
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $params = array(
            'extAttr' => '111',
            'externalSysCode' => '111',
            'externalSysType' => '111',
            'ordinal' => 0,
            'organUnitCode' => '111',
            'organUnitFullName' => '111',
            'organUnitName' => 'web前端部',
            'organUnitType' => '111',
            'parentId' => 1475148557166943,
        );
        $errMsg = "";
        $result = QiToon::createOrganUnit($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function updateAction()
    {
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $params = array(
            'extAttr' => '111',
            'externalSysCode' => '123',
            'externalSysType' => '111',
            'ordinal' => 0,
            'id'=>1475148557166968,
            'organUnitCode' => '123',
            'organUnitFullName' => '111',
            'organUnitName' => 'web前端部',
            'organUnitType' => '111',
            'parentId' => 1475148557166943,
        );
        $errMsg = "";
        $result = QiToon::updateOrganUnit($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function listAction()
    {
        $params = -1;
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $errMsg = "";
        $result = QiToon::getOrganUnit($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function listPageAction()
    {
        $params = 1475148557166943;
//        $curPage = 1;
//        $pageSize = 2;
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $errMsg = "";
        $result = QiToon::getOrganUnitPage($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }

    public function delAction()
    {
        $params = 123;
        $accessToken = Act_QtOauth::getAccessToken(55465);
        $errMsg = "";
        $result = QiToon::delOrganUnit($accessToken,$params,$errMsg);
        print_r(json_encode($result));
    }
}