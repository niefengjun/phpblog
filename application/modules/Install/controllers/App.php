<?php
/**
 * App信息
 * @author lifuqiang
 *
 */
class AppController extends Controller {
    
    /**
     * toon开放平台入口地址
     */
    public function oauthAction() {
        
        $code = trim($this->getRequest()->getQuery('code'));
        
        Fn::writeLog("code = {$code}", 'daily.log');
        
        $errMsg = '';
        $retData = Toon::getOrgAccessToken('daily', $code, $errMsg);
        
        if (false === $retData) {
            Fn::writeLog("获取AccessToken失败: $errMsg", 'daily.log');
        }
        
        Fn::writeLog("AccessInfo\t" . var_export($retData, true), 'daily.log');
        
        //获取feedId
        $userName = $retData['username'];
        $uid      = $retData['uid']; 
        
        
        echo "success\t uid={$uid}\t userName={$userName}";
        exit();
    }
    
    /**
     * 企通平台介绍应用
     */
    public function introAction() {
        $this->_dispatcher->enableView();
        
        return true;
    }
}