<?php
/**
 * 企通App
 * @author lifuqiang
 *
 */
class QtappController extends Controller
{
    public function introAction() 
    {
         $this->_dispatcher->enableView();

         return true;
    } 

    /**
     * 授权跳转
     */
    public function oauthAction($orgId = 0)
    {
//         $orgId = intval($orgId);
        
//         if (! $orgId) {
//             exit("安装错误");
//         }
        
        $appKey     = $this->_cnf['qtapp.daily.appKey'];
        $appSecret  = $this->_cnf['qtapp.daily.appSecret'];
        
        $errMsg= '';
        $appAToken = QiToon::getAppAToken($appKey, $appSecret, $errMsg);
        
        if (false === $appAToken) {
            exit("获取AppAToken错误： " . $errMsg);
        }
        
        $apiUrl = $this->_cnf['qtapp.authApiUrl'];
        
        $params = [
            'app_key'   => $appKey, 
            'response_type' => 'code',
            'scope' => 'app_buy',
            'state' => 'STATE',
            'app_token' => $appAToken, 
            'redirect_uri' => urlencode($this->_cnf['qtapp.daily.callback'])
        ];
        
        $redirectUrl = $apiUrl . '?' . http_build_query($params);
        
        Fn::writeLog("redirectUrl\t" . $redirectUrl, 'daily.log');
        
        header("Location:" . $redirectUrl);
        exit;
    }
    
    /**
     *  授权回调地址
     *  callback
     */
    public function callbackAction()
    {
        $appToken = trim($this->getRequest()->getQuery('appToken'));
        $rToken   = trim($this->getRequest()->getQuery('rToken'));
        $orgId    = trim($this->getRequest()->getQuery('orgId'));
        
        //存储数据
        $request = $this->getRequest()->getQuery();
        Fn::writeLog("callback\t" . var_export($request, true), 'daily.log');
        echo $appToken . "<br />";
        
        echo $rToken . "<br />";
        echo $orgId . "<br />"; 
        
        echo "<pre>";
        var_dump($_GET);
        echo "</pre>";
        
        exit;
    }
}