<?php
/**
 * 企通Oauth认证
 * @author lifuqiang
 *
 */
class Act_QtOauth
{
    const REDIS_STRING = 'main';
    const REDIS_PREFIX = 'Qt_Daily_Oauth::';
    
    /**
     * 获取appAtoken
     * @return boolean|Ambigous <boolean, mixed>
     */
    public static function getAppAToken()
    {
        $dailyInfo = Yaf_Registry::get('appConfig')->get('qtapp.daily');
        
        if (! $dailyInfo) {
            return false;
        }
        
        $errMsg = '';
        $appAToken = QiToon::getAppAToken($dailyInfo['appKey'], $dailyInfo['appSecret'], $errMsg);
        
        return $appAToken;
    }
    
    /**
     * 获取AccessToken
     * @param unknown $orgId
     * @return boolean|unknown
     */
    public static function getAccessToken($orgId)
    {
        $orgId = intval($orgId);
        
        if (! $orgId) {
            return false;
        }
        
        $mcKey = self::REDIS_PREFIX . 'AccessToken:' . $orgId;
        $accessToken = RedisClient::instance(self::REDIS_STRING)->get($mcKey);
        
        if (! $accessToken) {
            //到数据库中获取rToken
            $orgAuthObj = new OrgauthModel();
            $rToken = $orgAuthObj->getRTokenByOrgId($orgId);
            
            if (! $rToken) {
                return false;
            }
            
            $appAToken = self::getAppAToken();
            if (! $appAToken) {
                return false;
            }
            
            $errMsg = '';
            $tokenInfo = QiToon::getAccessTokenInfo($appAToken, $rToken, $errMsg);
            
            if (false === $tokenInfo) {
                return false;
            }
            
            //将信息更新到oauth认证库中
            $upRrow = [
                'accessToken' => $tokenInfo['accessToken'], 
                'rToken'      => $tokenInfo['rToken'],
            ];
            
            $orgAuthObj->updateInfoByOrgId($upRrow, $orgId);
            
            //设置7200缓存
            $accessToken = $tokenInfo['accessToken'];
            RedisClient::instance(self::REDIS_STRING)->setex($mcKey, 7200, $accessToken);
        }
        
        return $accessToken;
    }  
}