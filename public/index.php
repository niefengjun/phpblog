<?php
/**
 * 框架入口文件
 * @description Yaf 入口文件
 * @author zhaowei
 * @version 2016-05-24
 */
error_reporting(E_ALL);
define( "APP_PATH",  realpath( dirname(__FILE__) . '/../' ) );
define('APP_ENV', 'product');

//定义全局library
ini_set('yaf.library', APP_PATH . '/application/library');

$app  = new Yaf_Application( APP_PATH . "/conf/application.ini", APP_ENV);
$app->bootstrap()->run();
