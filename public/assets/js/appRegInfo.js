var appRegInfo = {
    appId: 642,
    // updateBaseUrl: baseUrl + "/set.html",
    // deleteBaseUrl:  baseUrl + "/plugin/delete/callback",
    // logintype: 0,
    // autoShare: 1,// 分享
    installInfo: {//安装应用
        "title": "日志",
        "type":"日志",
        "namespace": "shitoon.dev.systoon.com",
        "icon": "/asserts/image/logo.png",
        "introduction": "上班必备利器",
        "developer": "北京思源互联科技有限公司",
        "version": "1.1",
        "publishTime":"2016-06-06 20:00",
        "updateTime": "2016-09-22 20:00",
        "updateInfo": "上班必备利器",
        "canSetPubStatus": 1,//是否可设置公开:0不可，1可以
        "canSetShareStatus": 1,//是否可设置分享:0不可，1可以
        "defaultPubStatus": 1,//默认公开状态：0不公开，1公开
        "defaultShare": 1, // 默认分享状态：0不分享，1分享
        "afShow": 1,
        "ffShow": 1
    }

};
