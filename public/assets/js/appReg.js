/**
 * Created by 135946 on 2016/7/20.
 */
 // 'seller':"http://p100.orgoauth.systoon.com/oauth/authorize?"
var oauthUrl = 'http://orgoauth.systoon.com/oauth/authorize';
var info;
window.alert = function (msg, delay) {//所有弹出的提示信息
    if (typeof(delay) == "undefined") {
        delay = 3000;
    }
    var $info = $("#__dialog__");
    if ($info.length == 0) {
        $info = $('<div id="__dialog__"><div></div></div>');
        $info.css({
            "width": "100%",
            "position": "absolute",
            "bottom": "0.8666667rem",
            //"left":'60px',
            "text-align": "center",
            "z-index": 100
            //"font-size":"0.1866667rem"
        });
        $info.find("div").css({
            "background-color": "#333",
            "color": "#fff",
            "max-width": "2.5rem",
            "display": "inline-block",
            "border-radius": "0.13rem",
            "padding": "0.36666667rem",
            'opacity': 1,
            "text-align": "center"
        }).addClass("ui-dialog-blur");

        $("body").append($info);
    }
    $info.find("div").html(msg);
    $info.show();
    clearTimeout($info.data("timer"));
    var timer = setTimeout(function () {
        $info.removeClass("__dialog__fadeout");
        $info.hide();
    }, delay);
    $info.addClass("__dialog__fadeout");
    $info.data("timer", timer);
};
window.onload = function () {
    info = appRegInfo.installInfo;
    appId = appRegInfo.appId;
    state = appRegInfo.state;
    $("#icon").attr("src", info.icon);
    $("#title").html(info.title);
    $("#setTitle").val(info.title);
    $("#introduction").html(info.introduction);
    $("#developer").html(info.developer);
    $("#version").html(info.version);
    // var publishDate = new Date(info.publishTime);
    // var publishStr = publishDate.getFullYear() + "-" + publishDate.getMonth() + "-" + publishDate.getDay() + " " + publishDate.getHours() + ":" + publishDate.getMinutes();
    // $("#publishTime").html(publishStr);
    $("#publishTime").html(info.publishTime);
    // var date = new Date(info.updateTime);
    // var format = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDay() + " " + date.getHours() + ":" + date.getMinutes();
    // $("#updateTime").html(format);
    $("#updateTime").html(info.updateTime);
    $("#updateInfo").html(info.updateInfo);
    $("#indexPage").show();

    if(info.canSetPubStatus){
        $("#pubSlip").show();
        $("#pubTips").show();
    }
    if(info.canSetShareStatus){
        $("#shareSlip").show();
        $("#shareTips").show();
    }
    if(info.defaultPubStatus === 0){
        slipBtn(document.getElementById("pubBtn"));
    }
    if(info.defaultShare === 0){
        slipBtn(document.getElementById("shareBtn"));
    }
};
//获取QS
function getQueryStringByName(name) {
    var result = location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
    if (result == null || result.length < 1) {
        return "";
    }
    return result[1];
}

//添加按钮
function add(e) {
    // var indexPage = $('#indexPage');
    // var setPage = $('#setPage');
    // setPage.css({'display': 'block'});
    // setPage.addClass('animatestart sliderightin');
    // setTimeout(function () { //动画结束时重置class
    //     indexPage.css({'display': 'none'});
    //     setPage.removeClass('animatestart sliderightin');
    // }, 350);
    addNow(e);
}
//滑块
function slipBtn(e) {
    var dom = $(e);
    var circle = dom.next("a");
    if (dom.hasClass("kaiguan-guan-btn") || dom.hasClass("kaiguan-hui-btn")) {

        dom.removeClass("kaiguan-hui-btn");
        circle.removeClass("yuan-hui");

        dom.removeClass("kaiguan-guan-btn");
        circle.removeClass("yuan-guan");
        dom.addClass("kaiguan-kai-btn");
        circle.addClass("yuan-kai");
    } else {
        dom.addClass("kaiguan-guan-btn");
        circle.addClass("yuan-guan");
        dom.removeClass("kaiguan-kai-btn");
        circle.removeClass("yuan-kai");
    }
}
//应用标题输入
function titleOnInput(){

}
//马上添加
function addNow(e) {
    // var appName = $.trim($("#setTitle").val());
    // if (appName == "") {
    //     alert("请输入应用名称");
    //     return false;
    // }
    // if(appName.length > 5){
    //     alert("应用名称不能超过4个字符");
    //     return false
    // }
    // var afShow = 0;
    // var ffShow = 0;
    // if(info.canSetPubStatus === 1) {
    //     afShow  = $("#pubBtn").hasClass("kaiguan-guan-btn") ? 0 : 1;
    //     ffShow = $("#pubBtn").hasClass("kaiguan-guan-btn") ? 0 : 1;
    // }else{
    //     afShow = info.afShow;
    //     ffShow = info.ffShow;
    // }
    // var shareFlag = 0;
    // if(info.canSetShareStatus === 1){
    //     shareFlag = $("#shareBtn").hasClass("kaiguan-guan-btn")?0:1;
    // }else{
    //     shareFlag = 0;
    // }

    function buildQs(params) {
        var str = [];
        for(var p in params) {
            str.push(p + "=" + encodeURIComponent(params[p]));
        }
        return str.join("&");
    }
    // var redirectQSObj = {
    //     namespace:info.namespace,
    //     afShow:afShow,
    //     ffShow:ffShow,
    //     autoShare:shareFlag,
    //     title:appName
    // };
    //参数
    var params = {
        client_id:appId,
        response_type:"code",
        scope:"toonapi_userinfo",
        redirect_uri:window.location.origin+'/install/app/oauth',
        key:getQueryStringByName("key"),
        // state:state
    };
    location.href = oauthUrl + '?' + buildQs(params);
}
